import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import Header from '../../components/Header/Header'
import moment from 'moment'

function LerMais() {

    const [ lermais, setLermais ] = useState({})
    const { id } = useParams()
    let data = moment(lermais.data).format("DD/MM/YYYY");
    
    useEffect(() => {
        axios.get(`https://localhost:5001/lancamento/${id}`)
        .then((response) => {
            setLermais(response.data)
        })

    }, [])

    return (
        <div>
            <Header />
            
            <main>
                <div className="cards">

                    <div className="card" >

                        <header>
                            <h2>{lermais.titulo}</h2>
                        </header>

                        <div className="line"></div>

                        <p>R$ {lermais.valor}</p>
                        <p>{lermais.descricao}</p>
                        <p>{data}</p>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default LerMais