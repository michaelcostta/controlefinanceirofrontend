import React from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Header from "../../components/Header/Header";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

import './post.css'

const validationPost = yup.object().shape({
    titulo: yup.string().required("O Título é obrigatório.").max(30, "O Título precisa ter menos de 30 caracteres."),
    descricao: yup.string().required("A Descrição é obrigatória.").max(300, "A Descrição precisa ter menos de 300 caracteres."),
    valor: yup.number().typeError("O Valor é obrigatório.").required().max(1000000, "O Valor precisa ser menos de R$ 1000000."),
    data: yup.date().typeError("A Data é obrigatória.").required(),
    ehreceita: yup.bool().typeError("Selecione uma opção.").required()
})

function Post() {

    let history = useHistory()

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(validationPost)
    })

    const onSubmit = data => axios.post("https://localhost:5001/lancamento", data)
    .then(() => {
        window.alert("Enviado com sucesso!")
        history.push("/")
    })
    .catch(() => {
        window.alert("Erro ao enviar.")
    })

    return(
        <div>
            <Header/>

            <main>
                <div className="card-post">
                    <h1>Criar Lançamento</h1>
                    <div className="line-post"></div>

                    <div className="card-body-post">
                        <form onSubmit={handleSubmit(onSubmit)}>

                            <div className="fields">
                                <label>Título</label>
                                <input className="input-padrao" type="text" name="titulo" {...register("titulo")} ></input>
                                <p className="error-message">{errors.titulo?.message}</p>
                            </div>

                            <div className="fields">
                                <label>Descrição</label>
                                <textarea type="text" name="descricao" {...register("descricao")}></textarea>
                                <p className="error-message">{errors.descricao?.message}</p>
                            </div>

                            <div className="fields">
                                <label>Valor</label>
                                <input className="input-padrao" type="number" name="valor" {...register("valor")}></input>
                                <p className="error-message">{errors.valor?.message}</p>
                            </div>

                            <div className="fields">
                                <label>Data</label>
                                <input className="input-padrao" type="date" name="data" {...register("data")}></input>
                                <p className="error-message">{errors.data?.message}</p>
                            </div>

                            <div className="fields">
                                <p>Selecione uma opção:</p>
                                <label className="label-radio-receita"><input className="input-radio" type="radio" name="radio" value="true" {...register("ehreceita")}></input>Receita</label>
                                <label className="label-radio" ><input className="input-radio" type="radio" name="radio" value="false" {...register("ehreceita")}></input>Despesa</label>
                                <p className="error-message">{errors.ehreceita?.message}</p>
                            </div>

                            <div className="btn-post">
                                <button type="submit">Enviar</button>
                            </div>

                        </form>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default Post