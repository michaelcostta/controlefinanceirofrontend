import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import HeaderMain from "../../components/HeaderMain/HeaderMain";
import More from "../../images/more.svg"
import ArrowBack from '../../images/arrow_back_black_24dp.svg'
import ArrowForward from '../../images/arrow_forward_black_24dp.svg'
import './feed.css'

function Feed() {

    let history = useHistory()
    const [ posts, setPosts ] = useState([])

    useEffect(() => {
        axios.get("https://localhost:5001/lancamento")
        .then((response) => {
            setPosts(response.data)
        })
        .catch(() => {
            console.log("Falhou!")
        })
    }, [])

    function deletePost(id) {
        axios.delete(`https://localhost:5001/lancamento/${id}`)
        .then(() => {
            window.alert("Deletado com sucesso!")
        })
        setPosts(posts.filter(post => post.id !== id))
    }

    return(
        <div>
           <HeaderMain/>

            <main>
                <div className="cards">

                    {posts.map((post, key) => {

                        if (post.ehReceita) {
                            return (

                                <div className="card" key={key}>
                                    
                                    <header>
                                        <h2>{post.titulo}</h2>
                                        <img src={ArrowForward} style={{width: '35px', marginLeft: '250px'}} alt="receita"/>
                                        <img src={More} alt="imagem-mais"/>
                                    </header>
    
                                    <div className="line"></div>
    
                                    <p>R$ {post.valor}</p>
    
                                    <div className="btns">
                                        <div className="btn-edit">
                                            <Link to={{ pathname: `/edit/${post.id}`}}>
                                                <button>Editar</button>
                                            </Link>
                                        </div>

                                        <div className="btn-readmore" >
                                            <Link to={{pathname: `/lermais/${post.id}` }} >
                                            <button>Ler mais</button>
                                            </Link>
                                        </div>
    
                                        <div className="btn-delete">
                                            <button onClick={() => deletePost(post.id)}>Deletar</button>
                                        </div> 
                                    </div>
                                </div>
                            )
                        } else {
                            return (
                                <div className="card" key={key}>
                                    
                                    <header>
                                        <h2>{post.titulo}</h2>
                                        <img src={ArrowBack} style={{width: '35px', display: 'flex'}} alt="despesa"/>
                                        <img src={More} alt="imagem-mais"/>
                                    </header>
    
                                    <div className="line"></div>
    
                                    <p>R$ {post.valor}</p>
    
                                    <div className="btns">
                                        <div className="btn-edit">
                                            <Link to={{ pathname: `/edit/${post.id}`}}>
                                                <button>Editar</button>
                                            </Link>
                                        </div>

                                        <div className="btn-readmore" >
                                            <Link to={{pathname: `/lermais/${post.id}` }} >
                                            <button>Ler mais</button>
                                            </Link>
                                        </div>
    
                                        <div className="btn-delete">
                                            <button onClick={() => deletePost(post.id)}>Deletar</button>
                                        </div> 
                                    </div>
                                </div>
                            )
                        }
                    })}
               </div>
           </main>
        </div>
    )
}

export default Feed