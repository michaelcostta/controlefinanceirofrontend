import React from "react";
import { Link } from "react-router-dom";
import './HeaderMain.css'

function HeaderMain() {
    return(

        <header>
            <div className="container">
                <div className="logo">
                    <h1>Controle Financeiro</h1>
                </div>
                <div className="btn-novoLancamento">
                    <Link to="/post">
                        <button>Novo Lançamento</button>
                    </Link>
                </div>
            </div>
        </header>
    )
}

export default HeaderMain