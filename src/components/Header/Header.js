import React from "react";
import { Link } from "react-router-dom";
import Back from '../../images/back-button.svg'

function Header() {
    return(

        <header>
            <div className="container">
                <Link to="/">
                <img src={Back} style={{width: '35px'}} alt="botão-voltar"/>
                </Link>
            </div>
        </header>
    )
}

export default Header